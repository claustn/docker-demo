#Open Docker Registry
https://hub.docker.com/_/microsoft-powershell

#Show list of existing images

docker images

#Show :latest gotcha
docker run -it mcr.microsoft.com/powershell:latest
docker history mcr.microsoft.com/powershell:latest

#Pull down and run latest image
docker run mcr.microsoft.com/powershell:7.0.0-preview.1-alpine-3.8


