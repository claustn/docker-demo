﻿Push-Location 'C:\PSConf\Docker\1 Create Container'

# Create Docker image
docker build -t psconf .

#So what happened ?

#Docker File

#list docker images

docker images

# Let is try to remove the image

docker image rm psconf --force

#list docker images

docker images

#lets is rebuild it
cls
docker build -t psconf .

#Docker history

docker history psconf


#docker inspect - Get detailed information about docker

docker inspect psconf

#run container

docker run psconf

#What happened to our container ?

#docker run interactively (Will not work in ISE)

docker run -it psconf

# Add layer to Docker File (Edit dockerfile)


docker build -t psconf2 .


docker history psconf2


