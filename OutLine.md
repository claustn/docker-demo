Docker Session

1. Intro
   1. What/Why (Unity Anecdote)
   2. Disclaimer
      1. This is not about writing PowerShell code
      2. Lots of Docker stuff
      3. Not a session on How to write Pester tests
2. What is Docker Containers
   1. Images
      1. A Docker image is a file, comprised of multiple layers, used to execute code in a Docker container. An image is essentially built from the instructions for a complete and executable version of an application, which relies on the host OS kernel
      2.  Container images become containers at runtime and in the case of Docker containers - images become containers when they run on Docker Engine
   2. Containers
      1. A container is a standard unit of software that packages up code and all its dependencies so the application runs quickly and reliably from one computing environment to another. It also isolates code between container
   3. Layers
   4. Docker File
   5. Docker-Compose
3. Add/Run scripts
   1. Copy Files
   2. Mount Volumes
4. Run example Pester tests
   1. Single Container
   2. Using Docker Compose
   3. Output reports
5. Run scripts in CI pipeline
   1. Tell why Gitlab was chosen
      1. Gitlab Public
      2. Gitlab Internal
   2. Run scripts in Gitlab CI container
      1. Different OS container
      2. VM runners
         1. Show Container install ?
      3. Collect data across environments

6. VSCode Remote Development
   1. Talk about the different folders
      1. .devcontainer
      2. .vscode
      3. Show container build